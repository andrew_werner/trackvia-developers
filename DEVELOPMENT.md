## Developing for TrackVia Developers Website

Please follow the instructions below to complete setup for local development of the TrackVia Developer site.

### Install Node

We prefer using [nvm](https://github.com/nvm-sh/nvm) for Node installation and version management, but plenty of options exist.

**Install NVM:**

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

**Install and use Node version compatible with project:**

```bash
nvm install 18.1.0
nvm use 18.1.0
```

### Install pnpm

We prefer using [pnpm](https://pnpm.io/installation) for saving disk space and boosting installation speeds.

**Install pnpm using npm:**

```
npm install -g pnpm
```

**or via the standalone script:**

```bash
curl -fsSL https://get.pnpm.io/install.sh | sh -
```

### Install dependencies

First, run:

```
pnpm i
``` 
to install the necessary dependencies.

### Run site

Then, run: 

```
pnpm dev
``` 

to start the development server and visit `localhost:3000`.

## License

This project is licensed under the [MIT License](/LICENSE).

## Thank you!

The TrackVia team thanks you for your contribution to improve the overall developer experience for all developers working with the TrackVia Platform. We appreciate you being a part of our community! :tada:
