openapi: 3.0.0
info:
  title: TrackVia OpenAPI
  description: This is the OpenAPI specification for the TrackVia open api.
  version: 1.0.0
  termsOfService: https://trackvia.com/general-terms/
  contact:
    email: integrations@trackvia.com
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
servers:
  - url: https://{domain}/openapi
    variables:
      # https://github.com/swagger-api/swagger-ui/issues/4740
      domain:
        default: go.trackvia.com
        description: The domain of your TrackVia account.
security:
  - accessToken: []
  - userKey: []
  - accountId: []
tags:
  - name: Users
    description: User operations
  - name: Apps
    description: Application operations
  - name: Views
    description: View operations
  - name: Integrations
    description: Microservice operations
  - name: Records
    description: Record operations
  - name: Files
    description: File operations
paths:
  /users:
    get:
      summary: Get a list of users for an account
      tags:
        - Users
      parameters:
        - in: query
          name: start
          required: false
          schema:
            type: integer
          description: The page of users to return
        - in: query
          name: max
          required: false
          schema:
            type: integer
          description: The number of users per page
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
    post:
      summary: Create a user for an account
      tags:
        - Users
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                firstName:
                  type: string
                lastName:
                  type: string
                timeZone:
                  type: string
              required:
                - email
                - firstName
                - lastName
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordWithFieldsResponse'
        '400':
          description: Bad Request
        '409':
          description: Conflict
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /apps:
    get:
      summary: Returns a list of applications in an account
      tags:
        - Apps
      parameters:
        - in: query
          name: name
          required: false
          schema:
            type: string
          description: The name of the application to search for
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiAppResponse'
  /views:
    get:
      summary: Returns a list of views in an account
      tags:
        - Views
      parameters:
        - in: query
          name: name
          required: false
          schema:
            type: string
          description: The name of the view to search for
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiViewListElementResponse'
  /views/{viewId}:
    get:
      summary: Returns a list of records in a view
      tags:
        - Views
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to retrieve
        - in: query
          name: start
          required: false
          schema:
            type: integer
          description: The page of records to return
        - in: query
          name: max
          required: false
          schema:
            type: integer
          description: The number of records per page
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /views/{viewId}/find:
    get:
      summary: Returns a list of filtered records in a view
      tags:
        - Views
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to retrieve
        - in: query
          name: q
          required: false
          schema:
            type: string
          description: The string to search the view for
        - in: query
          name: start
          required: false
          schema:
            type: integer
          description: The page of records to return
        - in: query
          name: max
          required: false
          schema:
            type: integer
          description: The number of records per page
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /integrations:
    get:
      summary: Returns a list of microservices in an account
      tags:
        - Integrations
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiMicroserviceResponse'
  /integrations/{microserviceId}/invoke:
    get:
      summary: Invoke a microservice with arbitrary query string params
      tags:
        - Integrations
      parameters:
        - in: path
          name: microserviceId
          required: true
          schema:
            type: integer
          description: The id of the view to retrieve
        - in: query
          name: param
          required: false
          schema:
            type: string
          description: Example of arbitrary value being passed to microservice
      responses:
        '200':
          description: Successful response body is defined by microservice
          content:
            application/json:
              schema:
                type: object
    post:
      summary: Invoke a microservice with arbitrary request body params
      tags:
        - Integrations
      parameters:
        - in: path
          name: microserviceId
          required: true
          schema:
            type: integer
          description: The id of the view to retrieve
      requestBody:
        description: Payload of arbitrary values being passed to microservice
        required: true
        content:
          application/json:
            schema:
              type: object
          multipart/form-data:
            schema:
              type: object
      responses:
        '200':
          description: Successful response body is defined by microservice
          content:
            application/json:
              schema:
                type: object
  /views/{viewId}/records:
    post:
      summary: Create a single record in a view
      tags:
        - Records
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OpenApiRecordCreateRequest'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /views/{viewId}/records/all:
    delete:
      summary: Delete all records in a view
      tags:
        - Records
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
      responses:
        '204':
          description: No Content
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /views/{viewId}/records/{recordId}:
    get:
      summary: Returns a single record from a view
      tags:
        - Records
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record to retrieve
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordWithFieldsResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
    put:
      summary: Updates a single record from a view
      tags:
        - Records
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record to update
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OpenApiRecordUpdateRequest'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
    delete:
      summary: Deletes a single record from a view
      tags:
        - Records
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record to retrieve
      responses:
        '204':
          description: No Content
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
  /views/{viewId}/records/{recordId}/files/{fieldName}:
    get:
      summary: Returns a file from a single record from a view
      tags:
        - Files
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record to retrieve
        - in: path
          name: fieldName
          required: true
          schema:
            type: string
          description: The field name of the image or document field to retrieve
      responses:
        '200':
          description: Ok
          content:
            '*/*':
              schema:
                type: string
                format: binary
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
    post:
      summary: Attach a file to a single record in a view
      tags:
        - Files
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view to use
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record to attach to
        - in: path
          name: fieldName
          required: true
          schema:
            type: string
          description: The field name of the image or document field to attach
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                file:
                  type: string
                  format: binary
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiRecordQueryResponse'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
    delete:
      summary: Delete a file on a single record in a view
      tags:
        - Files
      parameters:
        - in: path
          name: viewId
          required: true
          schema:
            type: integer
          description: The id of the view
        - in: path
          name: recordId
          required: true
          schema:
            type: integer
          description: The id of the record
        - in: path
          name: fieldName
          required: true
          schema:
            type: string
          description: The field name of the image or document field
      responses:
        '200':
          description: Ok
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OpenApiExceptionResponse'
components:
  securitySchemes:
    accessToken:
      type: http
      in: header
      scheme: bearer
    userKey:
      type: apiKey
      in: query
      name: user_key
    accountId:
      type: apiKey
      in: header
      name: account-id
  schemas:
    OpenApiRecordCreateRequest:
      type: object
      description: Request body to create a record
      items:
        type: object
        properties:
          data:
            type: array
            items:
              type: object
      example:
        data:
          - Field A: Some Text
            Field B: 12345
    OpenApiRecordUpdateRequest:
      type: object
      description: Request body to update a record
      items:
        type: object
        properties:
          data:
            type: array
            items:
              type: object
      example:
        data:
          - Field A: Some Text
            Field B: 12345
    OpenApiAppResponse:
      type: array
      description: Applications in account
      items:
        type: object
        properties:
          name:
            type: string
          id:
            type: integer
    OpenApiRecordWithFieldsResponse:
      type: object
      properties:
        structure:
          type: array
          description: Fields in the view
          items:
            type: object
            properties:
              name:
                type: string
              type:
                type: string
              required:
                type: boolean
              unique:
                type: boolean
              canRead:
                type: boolean
              canUpdate:
                type: boolean
              canCreate:
                type: boolean
              fieldMetaId:
                type: integer
              relationshipSize:
                type: integer
              displayOrder:
                type: integer
              propertyName:
                type: string
        data:
          type: object
          description: Record in the view
    OpenApiRecordQueryResponse:
      type: object
      properties:
        structure:
          type: array
          description: Fields in the view
          items:
            type: object
            properties:
              name:
                type: string
              type:
                type: string
              required:
                type: boolean
              unique:
                type: boolean
              canRead:
                type: boolean
              canUpdate:
                type: boolean
              canCreate:
                type: boolean
              fieldMetaId:
                type: integer
              relationshipSize:
                type: integer
              displayOrder:
                type: integer
              propertyName:
                type: string
        data:
          type: array
          description: Records in the view
          items:
            type: object
        totalCount:
          type: integer
          description: Number of records in the view
    OpenApiViewListElementResponse:
      type: array
      description: Fields in the view
      items:
        type: object
        properties:
          id:
            type: integer
          name:
            type: string
          applicationName:
            type: string
          default:
            type: boolean
    OpenApiMicroserviceResponse:
      type: array
      description: Microservices in account
      items:
        type: object
        properties:
          name:
            type: string
          microserviceId:
            type: integer
    OpenApiExceptionResponse:
      type: object
      description: Detailed error information
      items:
        type: object
        properties:
          errors:
            type: array
            items:
              type: object
          message:
            type: string
          name:
            type: string
          code:
            type: string
          stackTrace:
            type: string
      example:
        errors:
        message: The HTTP message was not readable.
        name: httpMessageNotReadable
        code: 400
