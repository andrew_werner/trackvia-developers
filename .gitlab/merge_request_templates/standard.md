# Description

Please provide a clear and concise description of the changes you have made to the documentation. 

# Motivation

Why did you make these changes? What problem were you trying to solve?

# Screenshots (optional)

If applicable, add screenshots to help explain your changes.

# Types of changes

Please tick the relevant boxes:
- [ ] Typo fix
- [ ] Minor formatting change
- [ ] Addition of new content
- [ ] Deletion of outdated content
- [ ] Other (please describe): 

# Checklist

- [ ] I have reviewed the documentation guidelines
- [ ] I have made a corresponding change to the documentation on the development branch
- [ ] I have added relevant tests (if applicable)

# Further comments

Is there anything else you would like to add?
