# Common Swagger Errors And How To Resolve Them

![Troubleshooting](/Vector_Images_Scoping_tile_1.svg)

When testing API actions, the most common errors are caused by: 
- incorrectly inputting information such as using the wrong URL for the domain
- not authorizing properly using the API Key and Auth Token
- not assigning the necessary TrackVia resources to the API user

The sections below identify the most common errors you may encounter while using Swagger and the steps to take to resolve them. 

## Common Errors While Using Swagger

### 1. Incorrect Domain

On the Swagger page, make sure to check the “domain” field at the top of the page. Note that it defaults to go.trackvia.com, which should work for most cases if your account is on the standard PRODUCTION environment. If your account is on the GOV or HIPAA environments, you will need to edit the domain field accordingly. If your account uses a custom subdomain, you can use the default go.trackvia.com, or enter your custom subdomain here, but note that it will not work if the custom subdomain is not entered exactly as it appears in your account. 

If you input the incorrect URL in the domain field and try to execute an action, you will receive an error message as seen below:

![Swagger Incorrect Domain](/swagger1.png)


### 2. Incorrectly Entering Authorization Information

Before you start working with Swagger or Postman, make sure you've created an API user, the API Key, and a valid Auth Token. You can read more about how to set these up in [this Help Center article](https://help.trackvia.com/hc/en-us/articles/8700168754843-API-Authentication-and-Access). 

In Swagger, once all of those have been created, note that you will need to click the “Authorize” button at the top of the page. If you have not authorized, the lock icon next to the Authorize button will be unlocked, and if you try to enter information such as a View ID, it will be grayed out and not allow you to click into the field as shown in the screenshots below.

![Authorize](/swagger2a.png)


![Get/views](/swagger2b.png)

When you click on the Authorize button at the top of the page, you will need to go to your TrackVia application, click on the person icon in the top right corner, open “My Account” and click on the “API Access” section where you can copy your API key and Auth Token, paste them into the fields shown below, and click “Authorize” next to each one.

![Available auths](/swagger2c.png)

Once you've entered the API Key and Auth Token values as above and clicked “Authorize”, you'll notice that the lock icon now appears locked. Now, when you find the action you want to try on the page, click the “Try it out” button and you can now enter values in the fields on the page. Finally, click “Execute” to get your results.


### 3. API User Does Not Have Access to Necessary TrackVia Resources

When working with Swagger, error messages will appear in the “Response body” section. 

**Keep in mind that whichever Auth Token you used to authorize on the Swagger page is connected to an API User, and you will only be able to access resources (such as Views) that have been assigned to the API User's role.** If the API User is set as a Super Admin, they will have access to all resources in the account. If you only want to access specific resources via the API, you will need to create a Role within each application in your account, assign the API User to the role, and assign whichever resources to that role that you want to access. 

For example, you are using the `GET /views/{viewID}` to return a list of records in a view, you enter a view ID, and after clicking “Execute” you see a 401 error message in the Response body saying `“DENIED!!! - View XXXX either does not exist or you do not have access to it.”` (See screenshot below) 

![Denied Error](/swagger3.png)

That means that either 
**a)** you incorrectly entered the View ID and it does not correspond to any views in your account, or
**b)** the API User associated with the Auth Token you used to authorize the Swagger page does not have the View in question assigned to their role, and so it cannot be accessed. 


### 4. Not Using the Correct API Call While Integrating With Other Systems

In TrackVia, the API calls are detailed below. Note that the example below is for the "Records" section:
- POST = Create Record
- DEL = Delete Records in a View
- GET = Get Record
- PUT = Update Record

If you are integrating with other systems, **be sure to refer to their API documentation as they may not use the API actions in the same way as TrackVia**, which can be confusing and cause errors.