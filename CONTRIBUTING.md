# Welcome to TrackVia Developers site contributing guide <!-- omit in toc -->

Thank you for considering to invest your time and efforts in contributing to TrackVia Developers! 

Any contribution you make will be reflected immediately on [the developer site](https://developer.trackvia.com) :rocket:. 

This guide provides an overview of contribution workflows for either creating content change requests or developing said changes.

## Looking to Add or Change Content?

Before you get started, **we highly recommend you review [TrackVia's GitLab Onboarding Guide](https://gitlab.com/groups/xvia/trackvia/-/wikis/home)** to familiarize yourself with GitLab concepts and how we define and track work. 

There's also step-by-step instructions for issue creation, specifically **[how do I create an issue?](https://gitlab.com/groups/xvia/trackvia/-/wikis/Guides/Issues#how-do-i-create-an-issue)**

### Create a new issue

If you spot a problem with the site or want to make an enhancement, [search if an issue already exists](https://gitlab.com/xvia/trackvia/product/software/trackvia-developers/-/issues). 

If a related issue doesn't exist, you can open a new issue using this project's [create issue form](https://gitlab.com/xvia/trackvia/product/software/trackvia-developers/-/issues/new). 

### Select your issue template

The following **Issue Templates** are preferred to be used when creating a new issue:

1. **Content Change**: Requesting add/change/removal to content
2. **Technical Change**: Requesting new feature or enhancement
3. **Defect**: A bug that needs to be resolved

Please populate the sections that appear for the selected issue template, and then create your issue!

Someone from the TrackVia team will follow up to collaborate, triage, prioritize, and confirm when the change is ready for review on the beta site.

## Looking to Develop, Manage, & Deploy Content?

To get an overview of the project and learn how to navigate our codebase with confidence and develop locally, follow the project [README](README.md). 

### Solve an issue

Scan through our [existing issues]() to find one that interests you. You can narrow down the search using `labels` as filters. As a general rule, we don't assign issues to anyone. If you find an issue to work on, you are welcome to open an MR with a fix.

### Making code changes

1. For **TrackVia employee contributions**, please _create a branch from the primary branch_. **For outside contributors**, please _fork the repository_. This allows you to make your changes without affecting the original project until we are all ready to merge them.

3. Follow installation instructions in the project README to run the site locally.

4. Create a working branch and start with your changes!

### Commit your update

Commit the changes once you are happy with them. Don't forget to self-review to speed up the review process :zap:.

### Create a Merge Request

When you're finished with the changes, create a merge request, also known as an MR:

- Fill the "Merge Request" template so that we can review your MR. This template helps reviewers understand your changes as well as the purpose of your pull request. 
- Don't forget to link the MR to the related issue if you are solving one.
- Once you submit your MR, a TrackVia team member will review your proposal. We may ask questions or request additional information.
- We may ask for changes to be made before an MR can be merged through merge request comments. You can apply suggested changes directly through the UI or make any other changes in your fork, then commit them to your branch.
- As you update your MR and apply changes, mark each conversation as resolved.
- If you run into any merge issues, feel free to collaborate with us directly on the merge request.

### Approve, merge, and publish your updates!

Congratulations :tada: The TrackVia team thanks you for your contribution.

Once your MR is merged, your contributions will be publicly visible on the [TrackVia Developers site](https://developer.trackvia.com). 