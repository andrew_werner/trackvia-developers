import { useTheme } from 'nextra-theme-docs';

const Logo = () => {
    const { theme } = useTheme();

    return (
        <>
            <div style={{
                display: 'flex',
                padding: '3px',
                height: '36px',
                width: '36px',
                backgroundColor: 'rgba(255, 255, 255, var(--tw-bg-opacity))',
            }}>
                <svg
                    width="30px"
                    height="30px"
                    viewBox="0 0 30 30"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <title>TrackVia</title>
                    <g id="Container" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <g id="Canvas" transform="translate(-165.000000, -65.000000)" fill="#202434" fillRule="nonzero">
                            <g id="TrackVia-Logo" transform="translate(165.000000, 65.000000)">
                                <rect id="Rectangle" x="0" y="0" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" x="10.5027639" y="0" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" x="20.997631" y="0" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" opacity="0.4" x="0" y="10.5027639" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" x="10.5027639" y="10.5027639" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" opacity="0.4" x="20.997631" y="10.5027639" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" opacity="0.4" x="0" y="20.997631" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" x="10.5027639" y="20.997631" width="9.00236904" height="9.00236904"></rect>
                                <rect id="Rectangle" opacity="0.4" x="20.997631" y="20.997631" width="9.00236904" height="9.00236904"></rect>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>

            <span style={{ marginLeft: '.5em', fontWeight: 800 }}>
                TrackVia Developers
            </span>

        </>
    );
};

export default Logo;
