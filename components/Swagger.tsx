import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';
import { useRouter } from 'next/router';

const Swagger = () => {
    const router = useRouter();
    const { basePath } = router;
    return (
        <SwaggerUI
            url={`${basePath}/openapi.yaml`}
            withCredentials={false}
            persistAuthorization={true}
            presets={[SwaggerUI.presets.apis]}
            plugins={[SwaggerUI.plugins.DownloadUrl]}
            docExpansion="none"
        />
    );
};

export default Swagger;
