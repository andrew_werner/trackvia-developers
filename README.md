# TrackVia Developers

Welcome to the repository containing the new, modernized site and home for TrackVia Developers at [developer.trackvia.com](developer.trackvia.com).

## Where do I track the latest progress on site issues?

:warning: **Please see currently reported [Issues](https://gitlab.com/trackvia/developer-website/trackvia-developers/-/issues)** for tracking ongoing work on the developer site.

## How do I contribute or

:warning: **Please see [our Contribution Guide](/CONTRIBUTING)** for instructions on how to contribute and request changes to the website content, documentation, and supporting developer tools.

## Local development & making site changes

Looking to contribute by making code changes to the website? Follow the steps below to get up and running quickly!

Contributions will be reviewed and worked through TrackVia's software development lifecycle before being published to the production site.

:warning: **Please review [our Development Guide](/DEVELOPMENT)** for installation instructions to setup for local development.
