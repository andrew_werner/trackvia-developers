import React from 'react'
import { DocsThemeConfig } from 'nextra-theme-docs'
import Logo from './components/Logo'

const config: DocsThemeConfig = {
  logo: Logo,
  logoLink: "/",
  project: {
    icon: <svg width="24" height="24" viewBox="0 0 256 256"><path fill="currentColor" d="m231.9 169.8l-94.8 65.6a15.7 15.7 0 0 1-18.2 0l-94.8-65.6a16.1 16.1 0 0 1-6.4-17.3L45 50a12 12 0 0 1 22.9-1.1L88.5 104h79l20.6-55.1A12 12 0 0 1 211 50l27.3 102.5a16.1 16.1 0 0 1-6.4 17.3Z"></path></svg>,
    link: 'https://gitlab.com/trackvia/developer-website/trackvia-developers',
  },
  useNextSeoProps() {
    return {
      titleTemplate: "%s - TrackVia Developers"
    }
  },
  head: (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="title" content="TrackVia Developers" />
      <meta property="og:title" content="TrackVia Developers" />
      <link rel="icon" href="/cropped-TrackVia_logo_tile_RGB_White_Blue-copy-192x192.png" sizes="192x192" />
      <meta name="description" content="Create, automate, and solve problems for your team on TrackVia. TrackVia offers endless ways to build standalone solutions or complex integrations to your existing software." />
      <meta property="og:description" content="Create, automate, and solve problems for your team on TrackVia. TrackVia offers endless ways to build standalone solutions or complex integrations to your existing software." />
    </>
  ),
  sidebar: {
    defaultMenuCollapseLevel: 1,
  },
  footer: {
    text: <span>
    MIT {new Date().getFullYear()} © <a href="https://trackvia.com" target="_blank">TrackVia, Inc.</a></span>
  },
  editLink: {
    component: () => null,
  },
  feedback: {
    content: () => null,
  },
}


export default config
