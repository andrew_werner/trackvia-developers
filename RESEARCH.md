# Links

Existing TrackVia docs:

https://miro.com/app/board/uXjVP3FwJy8=/

- [App Script Errors](https://docs.google.com/document/d/1x8pT8Xtm3Rg1GZEMyfyz3qYQC1EDnUwc2FjaZEW7nC8/edit) or https://help.trackvia.com/hc/en-us/articles/10097672942491-Troubleshooting-App-Script-Errors

Documentation:

- https://nextra.site/
- https://github.com/markdoc/markdoc

DX:
https://gitlab.com/groups/xvia/trackvia/product/software/-/epics/65

Site Map:
https://miro.com/app/board/uXjVP3FwJy8=/

Others:

https://airtable.com/developers

https://airtable.com/developers/extensions/guides/building-a-new-app

https://stripe.com/docs/no-code/get-started


Groovy: https://groovy-lang.org/documentation.html#gettingstarted